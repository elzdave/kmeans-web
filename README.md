# K-Means Clustering - Web Version

## General Information
* Authors      : David Eleazar [[elzdave@student.untan.ac.id](mailto:elzdave@student.untan.ac.id)] and teams
* Prog. Lang    : PHP 7.1.*
* Framework     : Laravel 5.8
* Database      : SQLite
* 3rd Party Lib :
  - PHP KMeans by Benjamin Delespierre [[benjamin.delespierre@gmail.com](mailto:benjamin.delespierre@gmail.com)]
  - Laravel Excel by Maatwebsite [(https://github.com/maatwebsite/Laravel-Excel)]

## System Requirements
* OS        : Windows 7,8,8.1,10 ; Linux-based OS (Ubuntu, CentOS, dsb)
* Memory    : 512Mb or higher
* Processor : 1GHz single-core or better
* Storage   : 1Gb min.
* Software  :
  - Composer
  - Git

## Usage
1. Clone this repository
2. Using Terminal/Command Prompt, ```cd``` to this project's folder
3. Copy ```.env.example``` to ```.env```
4. Make empty ```database.sqlite``` file on ```database``` folder inside project's folder
5. Run ```composer install```
6. Run ```php artisan key:generate``` and ```php artisan migrate``` to import database settings
7. To run app, run ```php artisan serve```

## Explanation
1. We need to prepare dataset file using Microsoft Excel or Google Spreadsheet to store 2-dimensional data. Please refer to ```example.dataset.xlsx``` file as example on how to fill up the dataset
2. Make sure dataset file is stored as Excel's file format
3. Run this project, then select "Upload berkas dataset" radio option to enable file upload menu
4. Browse and select your dataset file, with maximum size of 1 MB.
5. Enter your desired number of cluster, ranged from 2 to 9
6. Press "Proses !" button to process the dataset
7. This apps will do the following upon pressing the "Proses !" button
   1. The controller's ```store``` function will validate the uploaded data with predefined validation rules
   2.  If validation passed, then the controller try to import the uploaded file into database, else this function will return 'validation failed' in JSON
   3.  If controller succeeded to import the dataset to database, then the function redirect to ```result``` page with dataset table and centroid position table. If not, this function will throw Error Exception with JSON format

## Contribution
Feel free to lend your help to improve this apps !