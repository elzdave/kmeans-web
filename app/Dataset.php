<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dataset extends Model
{
    protected $table = 'datasets';
    protected $fillable = [
        'batch_no','x_value','y_value'
    ];
}
