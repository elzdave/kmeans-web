<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Dataset;
use App\Imports\DatasetImport;
use Maatwebsite\Excel\Facades\Excel;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'mode_selected' => 'required|string',
            'num_of_gen_data' => 'nullable|integer|max:999',
            'kmeans_source' => 'nullable|file|max:1024|mimes:xlsx,xls',
            'num_of_cluster' => 'required|string'
        ]);
        if (!$validator->fails()){
            try {
                if ($request->mode_selected == 'dataset_from_file' && $request->hasFile('kmeans_source')){
                    Excel::import(new DatasetImport, $request->file('kmeans_source'));
                    $batch = Dataset::select('batch_no')->orderBy('batch_no','desc')->first();
                    return redirect()->route('kmeans.result',[
                        'batch' => $batch['batch_no'],
                        'cluster_num' => $request->num_of_cluster
                    ]);
                } else {
                    $data['payload'] = 'failed';
                    return response()->json($data,200);
                }
            } catch (\Throwable $th) {
                $data['payload'] = 'failed with '.$th;
                return response()->json($data,200);
            }
        } else {
            $data['message'] = 'validation failed';
            $data['data'] = $request->all();
            return response()->json($data,200);
        }
    }

    public function getChartData($batch,$cluster_num)
    {
        $processed = $this->processResult($batch,$cluster_num);
        $json['points'] = $processed['points'];
        $json['clusters'] = $processed['clusters'];
        return response()->json($json,200);
    }

    public function result($batch,$cluster_num)
    {
        $processed = $this->processResult($batch,$cluster_num);
        return view('pages.result')
            ->with([
                'datasets' => $processed['data'],
                'clusters' => $processed['clusters'],
                'batch_number' => $batch,
                'num_of_cluster' => $cluster_num
            ]);
    }

    private function processResult($batch,$cluster_num)
    {
        $object = Array();
        $data = Dataset::where('batch_no','=',$batch)->get();
        $points = Array();
        foreach ($data as $datum) {
            array_push($points,[$datum->x_value,$datum->y_value]);
        }
        $clusters = Array();
        // create a 2 dimensional space and fill it
        if (count($points) > 0){
            $space = new \KMeans\Space(2);
            foreach ($points as $point) {
                $space->addPoint($point);
            }
            // resolve clusters with params
            $clusters = $space->solve($cluster_num,\KMeans\Space::SEED_DASV);
        }
        $object['data'] = $data;
        $object['clusters'] = $clusters;
        $object['points'] = $points;
        return $object;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
