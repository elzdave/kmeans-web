<?php

namespace App\Imports;

use App\Dataset;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class DatasetImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collections)
    {
        $last_batch = Dataset::select('batch_no')->orderBy('batch_no','desc')->first();
        if (is_null($last_batch)){
            $last_batch['batch_no'] = 0;
        }
        unset($collections[0]);
        foreach ($collections as $collection) {
            Dataset::create([
                'batch_no' => $last_batch['batch_no']+1,
                'x_value' => $collection[0],
                'y_value' => $collection[1],
            ]);
        }
    }
}
