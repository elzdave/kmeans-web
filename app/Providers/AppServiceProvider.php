<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //Define component alias here
        Blade::component('components.checkbox','checkbox');
        Blade::component('components.checkboxes','checkboxes');
        Blade::component('components.email','email');
        Blade::component('components.password','password');
        Blade::component('components.radio','radio');
        Blade::component('components.radios','radios');
        Blade::component('components.select','select');
        Blade::component('components.text','text');
        Blade::component('components.date','date');
        Blade::component('components.breadcrumb','breadcrumb');
        Blade::component('components.modal','modal');
        Blade::component('components.column','column');
        Blade::component('components.columns','columns');
        Blade::component('components.record','record');
        Blade::component('components.records','records');
        Blade::component('components.alert','alert');
        Blade::component('components.delete','delete');
        Blade::component('components.edit','edit');
        Blade::component('components.detail','detail');
        Blade::component('components.block','block');
        Blade::component('components.tabs','tabs');
        Blade::component('components.tab','tab');
        Blade::component('components.tabcontents','tabcontents');
        Blade::component('components.tabcontent','tabcontent');
        Blade::component('components.label','label');
        Blade::component('components.color','color');
        Blade::component('components.jadwaltext','jadwaltext');
        Blade::component('components.file','file');
        Blade::component('components.backbtn','backbtn');
    }
}
