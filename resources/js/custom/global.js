/**
 * K-means global JS functions
 */
//set base url here
var root_url = location.protocol + '//' + location.host;

//set Ajax to include the CSRF token in every request
$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

$('input[name=mode_select]').on('change',function(){
  let mode = $(this).val();
  $('#mode_selected').val(mode);
  if (mode == 'dataset_from_file') {
    // $('#dataset_random_value').hide();
    $('#dataset_file').show();
  } else {
    $('#dataset_file').hide();
    // $('#dataset_random_value').show();
  }
});

//'Jumlah cluster' input masking
$(document).ready(function(){
  $('#num_of_cluster').mask('Y',{
      'translation': {
      Y: {pattern: /[2-9]/}
    }
  });
  // $('#num_of_gen_data').mask('XYY',{
  //   'translation': {
  //     X: {pattern: /[1-9]/},
  //     Y: {pattern: /[0-9]/}
  //   }
  // });
});

//limit 'kmeans_source' file input length
$('body').on('change','#kmeans_source',function(){
  let size = this.files[0].size;
  if (size > 1024000){
    Swal.fire({
      title : 'Operasi Illegal',
      text : 'Ukuran tidak boleh dari 1 MB! Silakan input kembali',
      type : 'error'
    });
    $(this).val('');
  }
});

$('.table-datatable').DataTable({
  dom : 'lfrtp',
});

//Form submit
// $('#kmeans_form').submit(function(){
//   alert($('#mode_selected').val());
//   return false;
// });

//Chart.js
// function showChart(batch,numOfCluster){
//   let ctx = $('#kmeans_result');
//   let url = root_url+'/chart-data/'+parseInt(batch)+'/'+parseInt(numOfCluster);
//   $.get(url,function(data,status){
//     let points = [
//       {x : 1,y : 3},
//       {x : 4,y : 7},
//       {x : 2,y : 5}
//     ];
//     let clusters = data.cluster;
//     let resultChart = new Chart(ctx,{
//       type : 'line',
//       data : points,
//       options : {
//         pointStyle : 'circle',
//         xAxisID : 'x',
//         yAxisID : 'y'
//       }
//     });
//   });
// }