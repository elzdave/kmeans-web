@php
  $back_url = url()->previous();
  $curr_url = url()->current();
  if ($back_url != $curr_url) {
    session(['back_url' => $back_url]);
  }
@endphp

<div class="col-auto">
  <a href="{{(isset($url) && $url != '') ? $url : url(session()->get('back_url'))}}" class="btn btn-primary" style="border-radius: 50%;">
    <i class="fas fa-arrow-circle-left"></i>
  </a>
</div>