@php
  $styles = '';
  if (isset($hidden) && $hidden == true){
    $styles != '' ? $styles .= ' ' : '';
    $styles .= 'display: none;';
  }
  if (isset($style) && $style != ''){
    $styles != '' ? $styles .= ' ' : '';
    $styles .= $style;
  }
@endphp

<div {!!isset($id) && $id != null ? 'id="'.$id.'"' : ''!!} {!!isset($class) && $class != null ? 'class="'.$class.'"' : ''!!} {!!$styles != '' ? 'style="'.$styles.'"' : '' !!}>
  {{$slot}}
</div>