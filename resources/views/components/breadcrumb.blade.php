<!-- Breadcrumb-->
<div class="breadcrumb-holder">
  <div class="container-fluid">
    <ul class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{route('home')}}">{{$title}}</a></li>
      <li class="breadcrumb-item active">{{$subtitle}}</li>
    </ul>
  </div>
</div>