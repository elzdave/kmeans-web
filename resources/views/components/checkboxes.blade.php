<div class="form-group row">
  <label class="col-sm-2 form-control-label">{{isset($label) && $label != null ? $label : ''}} {!! isset($required) && $required == true ? '<span class="text-danger h4">*</span>' : '' !!}</label>
  <div class="col-sm-10">
    {{$slot}}
  </div>
</div>