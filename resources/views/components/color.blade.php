@php
  $attributes = '';
  if (isset($required) && $required == true){
    $attributes .= 'required';
  }
  if (isset($readonly) && $readonly == true){
    $attributes != '' ? $attributes .= ' ' : '';
    $attributes .= 'readonly';
  }
  if (isset($autofocus) && $autofocus == true){
    $attributes != '' ? $attributes .= ' ' : '';
    $attributes .= 'autofocus';
  }
  if (isset($disabled) && $disabled == true){
    $attributes != '' ? $attributes .= ' ' : '';
    $attributes .= 'disabled';
  }
@endphp

<div class="{{isset($inline) && $inline == true ? 'form-group' : 'form-group row'}}">
  @if (isset($label))
  <label id="label-{{$name}}" class="col-sm-2 form-control-label" for="{{$name}}">{{$label != '' ? $label : ''}}{!! isset($required) && $required == true ? '<span class="text-danger h4">*</span>' : '' !!}</label>
  @endif
  <div class="col-sm-10">
    <input type="color" class="form-control" name="{{$name}}" id="{{$name}}" value="{{!is_null(old($name)) ? old($name) : $value}}" {{$attributes}}>
    @if (isset($help_text))
    <span class="text-small text-gray help-block-none">{{$help_text}}</span>
    @endif
  </div>
</div>