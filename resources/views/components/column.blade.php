<th {{isset($id) ? 'id='.$id : ''}} {{isset($class) ? 'class='.$class : ''}}>
  {{$slot}}
</th>