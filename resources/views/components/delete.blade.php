<a href="#" data-href="{{ $url }}" class="btn btn-danger {{isset($class) && $class != '' ? $class : 'delete'}}">
    {{ $slot }}
</a>