<tr>
    <th>
        {{ $name }}
    </th>
    <td>
        {{ $slot }}
    </td>
</tr>