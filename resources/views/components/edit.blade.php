@php
    $attributes = '';
    if (isset($data_class) && $data_class != null){
        $attributes != '' ? $attributes .= ' ' : '';
        $dataClass = 'data-class="'.$data_class.'"';
        $attributes .= $dataClass;
    }
    if (isset($value) && $value != null){
        $attributes != '' ? $attributes .= ' ' : '';
        $dataValue = 'data-value="'.$value.'"';
        $attributes .= $dataValue;
    }
    if (isset($label) && $label != null){
        $attributes != '' ? $attributes .= ' ' : '';
        $dataLabel = 'data-label="'.$label.'"';
        $attributes .= $dataLabel;
    }
@endphp

@isset($modal)
    @if ($modal)
        <a href='#' data-href="{{$url}}" {!!$attributes!!} class="btn {{isset($btn_type) && $btn_type != null ? $btn_type : 'btn-warning'}} {{isset($css_class) && $css_class != null ? $css_class : 'edit' }}">
            {{ $slot }}
        </a>
    @endif
@else
    <a href="{{$url}}" class="btn btn-warning {{isset($class) && $class != null ? $class : '' }}">
        {{ $slot }}
    </a>
@endisset


