@php
  $attributes = '';
  if (isset($required) && $required == true){
    $attributes .= 'required';
  }
  if (isset($readonly) && $readonly == true){
    $attributes != '' ? $attributes .= ' ' : '';
    $attributes .= 'readonly';
  }
  if (isset($autofocus) && $autofocus == true){
    $attributes != '' ? $attributes .= ' ' : '';
    $attributes .= 'autofocus';
  }
  if (isset($disabled) && $disabled == true){
    $attributes != '' ? $attributes .= ' ' : '';
    $attributes .= 'disabled';
  }
  //set main div class and input class
  if (isset($material) && $material == true){
    $mainDivClass = 'form-group-material';
    $inputClass = 'input-material';
  } elseif (isset($inline) && $inline == true){
    $mainDivClass = 'form-group';
    $inputClass = 'mr-3 form-control';
  } else {
    $mainDivClass = 'form-group row';
    $inputClass = 'form-control';
  }
  //check whether there's any custom class defined in component
  if (isset($class) && $class != '') {
    $inputClass .= ' ';
    $inputClass .= $class;
  }
@endphp

<div class="{{$mainDivClass}}">
  @if ((!isset($material) || $material == false) && (!isset($inline) || $inline == false))
    <label id="label-{{$name}}" class="col-sm-2 form-control-label" for="{{$name}}">{{isset($label) ? $label : ''}}{!! isset($required) && $required == true ? '<span class="text-danger h4">*</span>' : '' !!}</label>
    <div class="col-sm-10">
  @elseif ((!isset($material) || $material == false) && (isset($inline) && $inline == true))
    <label id="label-{{$name}}" class="sr-only" for="{{$name}}">{{isset($label) ? $label : ''}}{!! isset($required) && $required == true ? '<span class="text-danger h4">*</span>' : '' !!}</label>
  @endif
  <input type="email" class="{{$inputClass}}" name="{{$name}}" id="{{$name}}" value="{{!is_null(old($name)) ? old($name) : $value}}" placeholder="{{isset($material) && $material == true ? '' : $placeholder}}" {{$attributes}}>
  @if (isset($help_text))
    <span class="text-small text-gray help-block-none">{{$help_text}}</span>
  @endif
  @if ((!isset($material) || $material == false) && (!isset($inline) || $inline == false))
    </div>
  @elseif ((isset($material) && $material == true))
    <label id="label-{{$name}}" class="label-material" for="{{$name}}">{{isset($label) ? $label : ''}}{!! isset($required) && $required == true ? '<span class="text-danger h4">*</span>' : '' !!}</label>
  @endif
</div>