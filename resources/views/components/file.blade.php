@php
  /**
   * Custom file component based on Bootstrap Filestyle 2.1.0
  */
  $attributes = '';
  if (isset($required) && $required == true){
    $attributes .= 'required';
  }
  if (isset($autofocus) && $autofocus == true){
    $attributes != '' ? $attributes .= ' ' : '';
    $attributes .= 'autofocus';
  }
  if (isset($multiple) && $multiple == true){
    $attributes != '' ? $attributes .= ' ' : '';
    $attributes .= 'multiple';
  }
@endphp

<div class="{{isset($inline) && $inline == true ? 'form-group' : 'form-group row'}}">
  @if (isset($label))
  <label id="label-{{$name}}" class="col-sm-2 form-control-label" for="{{$name}}">{{$label != '' ? $label : ''}}{!! isset($required) && $required == true ? '<span class="text-danger h4">*</span>' : '' !!}</label>
  @endif
  <div class="col-sm-10">
    <input type="file" class="filestyle" data-dragdrop="false" data-input="false" data-text="Pilih berkas" data-btnClass="btn-outline-secondary" data-disable="{{isset($disabled) && $disabled == true ? 'true' : 'false'}}" name="{{$name}}" id="{{$name}}" {{$attributes}} accept="{{$accept}}">
    @if (isset($help_text))
    <span class="text-small text-gray help-block-none">{{$help_text}}</span>
    @endif
  </div>
</div>