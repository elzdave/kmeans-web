<header class="header">
  <nav class="navbar">
    <div class="container-fluid">
      <div class="navbar-holder d-flex align-items-center justify-content-between">
        <div class="navbar-header"><a id="toggle-btn" href="#" class="menu-btn"><i class="icon-bars"> </i></a><a href="{{url('/')}}" class="navbar-brand">
            <div class="brand-text d-none d-md-inline-block"><span>{{$main_title}} </span><strong class="text-primary">{{$strong_title}}</strong></div></a>
        </div>
        <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
          {{-- Notifications dropdown --}}
          {{-- <li class="nav-item dropdown"> <a id="notifications" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link"><i class="fas fa-bell"></i><span class="badge badge-warning">12</span></a>
            <ul aria-labelledby="notifications" class="dropdown-menu">
              <li><a rel="nofollow" href="#" class="dropdown-item">
                  <div class="notification d-flex justify-content-between">
                    <div class="notification-content"><i class="fa fa-envelope"></i>You have 6 new messages </div>
                    <div class="notification-time"><small>4 minutes ago</small></div>
                  </div></a></li>
              <li><a rel="nofollow" href="#" class="dropdown-item">
                  <div class="notification d-flex justify-content-between">
                    <div class="notification-content"><i class="fa fa-twitter"></i>You have 2 followers</div>
                    <div class="notification-time"><small>4 minutes ago</small></div>
                  </div></a></li>
              <li><a rel="nofollow" href="#" class="dropdown-item">
                  <div class="notification d-flex justify-content-between">
                    <div class="notification-content"><i class="fa fa-upload"></i>Server Rebooted</div>
                    <div class="notification-time"><small>4 minutes ago</small></div>
                  </div></a></li>
              <li><a rel="nofollow" href="#" class="dropdown-item">
                  <div class="notification d-flex justify-content-between">
                    <div class="notification-content"><i class="fa fa-twitter"></i>You have 2 followers</div>
                    <div class="notification-time"><small>10 minutes ago</small></div>
                  </div></a></li>
              <li><a rel="nofollow" href="#" class="dropdown-item all-notifications text-center"> <strong> <i class="fa fa-bell"></i>view all notifications                                            </strong></a></li>
            </ul>
          </li> --}}
          {{-- Messages dropdown --}}
          {{-- <li class="nav-item dropdown"> <a id="messages" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link"><i class="fas fa-envelope"></i><span class="badge badge-info">10</span></a>
            <ul aria-labelledby="notifications" class="dropdown-menu">
              <li><a rel="nofollow" href="#" class="dropdown-item d-flex">
                  <div class="msg-profile"> <img src="img/avatar-1.jpg" alt="..." class="img-fluid rounded-circle"></div>
                  <div class="msg-body">
                    <h3 class="h5">Jason Doe</h3><span>sent you a direct message</span><small>3 days ago at 7:58 pm - 10.06.2014</small>
                  </div></a></li>
              <li><a rel="nofollow" href="#" class="dropdown-item d-flex">
                  <div class="msg-profile"> <img src="img/avatar-2.jpg" alt="..." class="img-fluid rounded-circle"></div>
                  <div class="msg-body">
                    <h3 class="h5">Frank Williams</h3><span>sent you a direct message</span><small>3 days ago at 7:58 pm - 10.06.2014</small>
                  </div></a></li>
              <li><a rel="nofollow" href="#" class="dropdown-item d-flex">
                  <div class="msg-profile"> <img src="img/avatar-3.jpg" alt="..." class="img-fluid rounded-circle"></div>
                  <div class="msg-body">
                    <h3 class="h5">Ashley Wood</h3><span>sent you a direct message</span><small>3 days ago at 7:58 pm - 10.06.2014</small>
                  </div></a></li>
              <li><a rel="nofollow" href="#" class="dropdown-item all-notifications text-center"> <strong> <i class="fa fa-envelope"></i>Read all messages    </strong></a></li>
            </ul>
          </li> --}}
          {{-- Menu dropdown --}}
          <li class="nav-item dropdown"><a id="admin-menu" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle"><span class="d-none d-sm-inline-block">Menu</span></a>
            <ul aria-labelledby="admin-menu" class="dropdown-menu">
              <li><a href="#" class="dropdown-item" id="change-pwd"><span>Ubah Password</span></a></li>
              <li><a href="#" class="dropdown-item" id="update-userdata"><span>Perbarui Data</span></a></li>
              {{-- <li><a rel="nofollow" href="#" class="dropdown-item"> <img src="img/flags/16/FR.png" alt="English" class="mr-2"><span>French                                                         </span></a></li> --}}
            </ul>
            {{-- Change password modal --}}
            @modal([
              'name'=>'change-pwd',
              'title' => 'Ubah Password'
            ])
              @slot('body')
                <div id="res-msg"></div>
                <form>
                  @password([
                    'label' =>'Password lama',
                    'placeholder' => 'Masukkan password lama Anda',
                    'name' => 'old_pwd',
                    'value' => '',
                    'material' => true
                  ])
                  @endpassword
                  @password([
                    'label' =>'Password baru',
                    'placeholder' => 'Masukkan password baru Anda',
                    'name' => 'new_pwd',
                    'value' => '',
                    'help_text' => 'Minimal 8 karakter',
                    'material' => true
                  ])
                  @endpassword
                  @password([
                    'label' =>'Konfirmasi Password baru',
                    'placeholder' => 'Masukkan password baru Anda',
                    'name' => 'new_pwd_conf',
                    'value' => '',
                    'material' => true
                  ])
                  @endpassword
                </form>
              @endslot
              @slot('footer')
                <button class="btn btn-primary" id="proceed-change-pwd">Lanjutkan</button>
                <button class="btn btn-secondary" id="btn-close-change-pwd" data-dismiss="modal">Batal</button>
              @endslot
            @endmodal
            {{-- Update userdata modal --}}
            @modal([
              'name'=>'update-userdata',
              'title' => 'Perbarui Data Pengguna'
            ])
              @slot('body')
                <div id="res-msg"></div>
                <form>
                  @text([
                    'name' => 'name',
                    'label' => 'Username',
                    'value' => '',
                    'placeholder' => '',
                    'required' => true,
                    'material' => true
                  ])
                  @endtext
                  @email([
                    'name' => 'email',
                    'label' => 'Email',
                    'value' => '',
                    'placeholder' => '',
                    'required' => true,
                    'material' => true
                  ])
                  @endemail
                </form>
              @endslot
              @slot('footer')
                <button class="btn btn-primary" id="proceed-update-userdata">Lanjutkan</button>
                <button class="btn btn-secondary" id="btn-close-update-userdata" data-dismiss="modal">Batal</button>
              @endslot
            @endmodal
          </li>
          {{-- Log out --}}
          <li class="nav-item">
            <a href="" class="nav-link logout" id="logout">
              <span class="d-none d-sm-inline-block">Logout</span>
              <i class="fas fa-sign-out-alt"></i>
            </a>
            {{-- Logout modal --}}
            @modal([
              'name' => 'logout-confirm',
              'title' => 'Konfirmasi Logout'
            ])
              @slot('body')
                <p>Apakah Anda yakin untuk keluar?</p>
              @endslot
              @slot('footer')
                <form id="logout-form" action="{{ route('logout') }}" method="POST">
                  @csrf
                  <button type="submit" class="btn btn-primary">Lanjutkan</button>
                </form>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
              @endslot
            @endmodal
          </li>
        </ul>
      </div>
    </div>
  </nav>
</header>