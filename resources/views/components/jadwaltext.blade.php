<div class="form-group">
  <div class="input-group">
    <input type="text" class="form-control form-control-sm jadwal-autocomplete" name="{{$name}}" id="{{$name}}" value="{{$value}}">
    <div class="input-group-append delete-jadwal" style="display: none;"><span class="btn-sm input-group-text">&#10006;</span></div>
    <input type="hidden" name="id-{{$name}}" id="id-{{$name}}" value="{{$hidden_value}}" data-slotjam="{{$slotjam}}" data-kelas="{{$kelas}}" data-guru="{{$guru}}" data-self="{{$self_id}}">
  </div>
</div>