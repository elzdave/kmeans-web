<label {!!isset($class) && $class != null ? 'class="'.$class.'"' : ''!!}>
  {{$slot}}{!! isset($required) && $required == true ? '<span class="text-danger h4">*</span>' : '' !!}
</label>