<div id="{{$name}}-modal" role="dialog" aria-hidden="true" class="modal fade text-left">
  <div role="document" class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h2 id="{{$name}}-modal-label" class="table-title">{{$title}}</h2>
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        {{$body}}
      </div>
      <div class="modal-footer">
        {{$footer}}
      </div>
    </div>
  </div>
</div>