@php
  $attributes = '';
  if (isset($required) && $required == true){
    $attributes .= 'required';
  }
  if (isset($checked) && $checked == true){
    $attributes != '' ? $attributes .= ' ' : '';
    $attributes .= 'checked';
  }
  if (isset($autofocus) && $autofocus == true){
    $attributes != '' ? $attributes .= ' ' : '';
    $attributes .= 'autofocus';
  }
  if (isset($disabled) && $disabled == true){
    $attributes != '' ? $attributes .= ' ' : '';
    $attributes .= 'disabled';
  }
  //set main div class and input class
  $mainDivClass = '';
  $inputClass = '';
  if (isset($custom) && $custom == true){
    $mainDivClass = 'i-checks';
    $inputClass = 'form-control-custom radio-custom';
  }
  //check whether there's any custom class defined in component
  if (isset($class) && $class != '') {
    $inputClass != '' ? $inputClass .= ' ' : '';
    $inputClass .= $class;
  }
@endphp

<div class="{{$mainDivClass}}">
  <input type="radio" class="{{$inputClass}}" name="{{$name}}" id="{{$id}}" value="{{$value}}" {{$attributes}}>
  <label id="label-{{$name}}" for="{{$id}}">{{$label}} {!! isset($required) && $required == true ? '<span class="text-danger h4">*</span>' : '' !!}</label>
</div>