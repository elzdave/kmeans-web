<div class="form-group row">
  <label class="col-sm-2 form-control-label">
    {{ $label }} {!! isset($required) && $required == true ? '<span class="text-danger h4">*</span>' : '' !!}
  </label>
  <div>
    {{ $slot }}
  </div>
</div>