<td {{isset($id) ? 'id='.$id : ''}} {!!isset($class) ? 'class="'.$class.'"' : ''!!}>
  @isset($url)
  <a href="{{ $url }}">
  @endisset
  {{ $slot }}
  @isset($url)
  </a>
  @endisset
</td>