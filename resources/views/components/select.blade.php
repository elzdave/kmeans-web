@php
  $attributes = '';
  if (isset($required) && $required == true){
    $attributes .= 'required';
  }
  if (isset($multiple) && $multiple == true){
    $attributes != '' ? $attributes .= ' ' : '';
    $attributes .= 'multiple';
  }
  if (isset($autofocus) && $autofocus == true){
    $attributes != '' ? $attributes .= ' ' : '';
    $attributes .= 'autofocus';
  }
  if (isset($disabled) && $disabled == true){
    $attributes != '' ? $attributes .= ' ' : '';
    $attributes .= 'disabled';
  }
  //label class for inline
  if (isset($inline) && $inline == true){
    $labelClass = 'sr-only';
    $divClass = 'col';
  } else {
    $labelClass = 'col-sm-2 form-control-label';
    $divClass = 'col-sm-10';
  }
@endphp

<div class="form-group row">
  <label id="label-{{$name}}" class="{{$labelClass}}" for="{{$name}}">{{$label}}{!! isset($required) && $required == true ? '<span class="text-danger h4">*</span>' : '' !!}</label>
  <div class="{{$divClass}}">
    <select name="{{$name}}" id="{{$name}}" class="form-control" {!! isset($size) ? 'size="'.$size.'"' : ''!!} {{$attributes}}>
      <option value="" {{ !isset($selected) ? 'selected' : '' }}>
        Pilih salah satu
      </option>
      @foreach ($options as $option)
      <option value="{{$option->id}}" {{ isset($selected) && $selected == $option->id ? 'selected' : '' }}>
        @isset($display)
          {{ $option[$display] }}
        @else
          {{ $option->getContent()}}
        @endisset
      </option>
      @endforeach
    </select>
    @if (isset($help_text))
      <span class="text-small text-gray help-block-none">{{$help_text}}</span>
    @endif
  </div>
</div>