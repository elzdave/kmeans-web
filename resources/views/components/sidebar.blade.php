<!-- Side Navbar -->
<nav class="side-navbar">
  <div class="side-navbar-wrapper">
    <!-- Sidebar Header    -->
    <div class="sidenav-header d-flex align-items-center justify-content-center">
      <!-- User Info-->
      <div class="sidenav-header-inner text-center"><img src="{{asset('img/logosmpsuster.png')}}" alt="logo" class="img-fluid rounded-circle">
        <h2 class="h5" id="active-user-name">{{Auth::user()->name}}</h2><span id="active-user-email">{{Auth::user()->email}}</span>
      </div>
      <!-- Small Brand information, appears on minimized sidebar-->
      <div class="sidenav-header-logo"><a href="index.html" class="brand-small text-center"> <strong>I</strong><strong class="text-primary">F</strong></a></div>
    </div>
    <!-- Sidebar Navigation Menus-->
    <div class="main-menu">
      <h5 class="sidenav-heading">Main</h5>
      <ul id="side-main-menu" class="side-menu list-unstyled">
        <li><a href="{{route('home')}}"><i class="fas fa-home"></i>Beranda</a></li>
        {{-- <li><a href="{{route('forms')}}"><i class="fab fa-wpforms"></i>Forms</a></li> --}}
        @can('index pegawai')
          <li><a href="{{route('pegawai.index')}}"><i class="fas fa-user"></i>Pegawai</a></li>
        @endcan
        @can('index guru')
          <li><a href="{{route('guru.index')}}"><i class="fas fa-chalkboard-teacher"></i>Guru</a></li>
        @endcan
        @can('index siswa')
          <li><a href="{{route('siswa.index')}}"><i class="fas fa-child"></i>Siswa</a></li>
        @endcan
        @can('index kelas')
          <li><a href="{{route('kelas.index')}}"><i class="fas fa-chalkboard"></i>Kelas</a></li>
        @endcan
        <li><a href="#jadwalPelajaran" aria-expanded="false" data-toggle="collapse"><i class="far fa-calendar-alt"></i>Jadwal Pelajaran</a>
          <ul id="jadwalPelajaran" class="collapse list-unstyled ">
            @can('index jadwal')
              <li><a href="{{route('jadwal.index')}}">Lihat Jadwal Pelajaran</a></li>
              <li><a href="{{route('jadwal.index.kelas')}}">Lihat Jadwal per Kelas</a></li>
              <li><a href="{{route('jadwal.index.guru')}}">Lihat Jadwal per Guru</a></li>
            @endcan
            @can('index guru mapel')
              <li><a href="{{route('guru-mapel.index')}}">Lihat Guru Mata Pelajaran</a></li>
            @endcan
            @can('index jam pelajaran')
              <li><a href="{{route('jam-pelajaran.index')}}">Lihat Jam Pelajaran</a></li>
            @endcan
            @can('show riwayat mengajar')
              <li><a href="{{route('jadwal.history')}}">Lihat Riwayat Mengajar</a></li>
            @endcan
          </ul>
        </li>
        @can('index misc')
          <li><a href="{{route('misc.index')}}"><i class="fab fa-bitcoin"></i>Data Lainnya</a></li>
        @endcan
      </ul>
    </div>
    @role('super-admin')
      <div class="admin-menu">
        <h5 class="sidenav-heading">Menu Administrasi</h5>
        <ul id="side-admin-menu" class="side-menu list-unstyled">
          @can('manage users')
            <li><a href="{{route('users.index')}}"><i class="fas fa-users"></i>Manajemen Pengguna</a></li>
          @endcan
        </ul>
      </div>
    @endrole
  </div>
</nav>
