<li class="nav-item">
  <a class="nav-link{{isset($active) && $active == true ? ' active' : ''}}" {!!isset($id) && $id != null ? 'id="'.$id.'"' : ''!!} href="#{{isset($href) && $href != null ? $href : ''}}" {!!isset($value) && $value != null ? 'data-value="'.$value.'"' : '' !!} data-toggle="tab" role="tab" aria-controls="{{isset($href) ? $href : ''}}" aria-selected="{{isset($active) && $active == true ? 'true' : 'false'}}">
    {{$slot}}
    @if(isset($color))
    <span class="tab-color" {!! isset($color) && $color != null ? 'style="background-color:'.$color.';"' : '' !!}></span>
    @endif
  </a>
</li>
