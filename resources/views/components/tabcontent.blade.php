<div class="tab-pane fade {{isset($active) && $active == true ? 'show active' : ''}}" {!!isset($id) && $id != null ? 'id="'.$id.'"' : ''!!} role="tabpanel" aria-labelledby="{{$id}}-tab">
  {{$slot}}
</div>