<ul class="nav {{isset($type) && $type != null ? 'nav-'.$type : 'nav-tabs'}} justify-content-center" role="tablist" {!!isset($id) && $id != null ? 'id="'.$id.'"' : ''!!}>
  {{$slot}}
</ul>