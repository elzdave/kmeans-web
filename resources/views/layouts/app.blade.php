<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>KMeans Clustering - @yield('title')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Application CSS-->
    <link rel="stylesheet" href="{{mix('css/app.css')}}">
    <!-- Vendor stylesheet-->
    <link rel="stylesheet" href="{{mix('css/vendor.css')}}">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{{mix('css/theme.css')}}">
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    {{-- Detect if JS is disabled in browser --}}
    <noscript>
      <style type="text/css">
        #js-enabled-area { display:none; }
      </style>
      @include('layouts.nojs')
    </noscript>
    <div id="js-enabled-area">
      @yield('main')
    </div>
    <!-- JavaScript files-->
    <script src="{{mix('js/app.js')}}"></script>
    <script src="{{mix('js/vendor.js')}}"></script>
    <script src="{{mix('js/theme.js')}}"></script>
    <script src="{{mix('js/custom.js')}}"></script>
    @stack('js')
  </body>
</html>