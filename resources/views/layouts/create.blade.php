@extends('layouts.app')

@php
  $back_url = url()->previous();
  $curr_url = url()->current();
  if ($back_url != $curr_url) {
    session(['back_url' => $back_url]);
  }
@endphp

@section('content')
  @include('components.header',[
    'main_title' => 'Create',
    'strong_title' => 'Content'
  ])
  @yield('breadcrumb')
  <section>
    <div class="container-fluid">
      <header></header>
      @if (session()->get('message'))
        @alert([
          'type' => session()->get('type'),
          'message' => session()->get('message')
        ])
        @endalert
      @endif
      <div class="card">
        <div class="card-header">
          <h1 class="table-title">Tambahkan @yield('card-title')</h1>
        </div>
        <div class="card-body">
          <form {!!isset($form_id) && $form_id != null ? 'id="'.$form_id.'"' : '' !!} action="{{isset($route_param) ? route($route,$route_param) : route($route)}}" method="POST" enctype="multipart/form-data">
            @method('POST')
            @csrf
            <p class="form-text mb-4"><span class="text-danger h4">*</span> = wajib diisi</p>
            @yield('inputs')
            <div class="row">
              <div class="col"></div>
              <div class="col-auto">
                <input type="reset" class="btn btn-dark" value="Reset" data-toggle="tooltip" data-placement="top" data-trigger="hover" title="Kembalikan input ke kondisi semula">
                <input type="submit" class="btn btn-primary" value="Simpan" data-toggle="tooltip" data-placement="top" data-trigger="hover" title="Simpan perubahan">
                <a href="{{url(session()->get('back_url'))}}" class="btn btn-danger" data-toggle="tooltip" data-placement="top" data-trigger="hover" title="Kembali ke halaman sebelumnya">Kembali</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
@endsection