@extends('layouts.app')

@section('content')
  @include('components.header',[
    'main_title' => 'Dashboard',
    'strong_title' => 'Home'
  ])
  @yield('breadcrumb')
  <section>
    <div class="container-fluid">
      <header></header>
      @if (session()->get('message'))
        @alert([
          'type' => session()->get('type'),
          'message' => session()->get('message')
        ])
        @endalert
      @endif
      @yield('subcontent')
    </div>
  </section>
@endsection