<div class="page login-page">
  <div class="container">
    <div class="form-outer text-center d-flex align-items-center">
      <div class="form-inner">
        <div class="logo text-uppercase"><span>Kmeans</span><strong class="text-primary">Clustering</strong></div>
        <div class="alert alert-info alert-block">
          Kami mendeteksi bahwa JavaScript <strong class="text-danger">dinonaktifkan</strong> pada browser ini.
          Aplikasi ini tidak dapat berfungsi dengan baik tanpa JavaScript.
          Oleh sebab itu, <strong class="text-primary">aktifkan</strong> JavaScript dan reload halaman ini.
        </div>
        <a href="" class="btn btn-primary btn-block">Reload</a>
      </div>
      <div class="copyrights text-center">
        <p>Design by <a href="https://bootstrapious.com/p/bootstrap-4-dashboard" class="external" target="_blank">Bootstrapious</a></p>
        <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
      </div>
    </div>
  </div>
</div>