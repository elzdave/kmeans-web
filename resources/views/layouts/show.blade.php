@extends('layouts.app')

@section('content')
  @include('components.header',[
    'main_title' => 'Dashboard',
    'strong_title' => 'Home'
  ])
  @yield('breadcrumb')
  <section>
    <div class="container-fluid">
      <header></header>
      @if (session()->get('message'))
        @alert([
          'type' => session()->get('type'),
          'message' => session()->get('message')
        ])
        @endalert
      @endif
      <div class="card">
        <div class="card-header">
          <h1 class="table-title">Detail @yield('card-title')</h1>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-hover">
              @yield('details')
            </table>
          </div>
          <div class="row">
            <div class="col"></div>
            <div class="col-auto">
              @canany(['edit pegawai','edit siswa','edit kelas','edit guru mapel','edit jam pelajaran'])
                @edit([
                  'url' => route($edit_route,[
                    'id' => $id
                  ])
                ])
                  <i class="fas fa-edit"></i>
                  Edit
                @endedit
              @endcanany
              <a href="{{route($index)}}" class="btn btn-danger">
                <i class="fas fa-angle-left"></i> Ke Halaman Utama
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection