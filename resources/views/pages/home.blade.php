@extends('layouts.app')

@section('title','Main')

@section('main')
  <div class="page login-page">
    <div class="container">
      <div class="form-outer text-center d-flex align-items-center">
        <div class="form-inner">
          <div class="logo text-uppercase text-center"><a href="{{route('kmeans.index')}}"><span>Kmeans</span><strong class="text-primary">Clustering</strong></a></div>
          @if (session()->get('message'))
            @alert([
              'type' => session()->get('type'),
              'message' => session()->get('message')
            ])
            @endalert
          @endif
          <p>Silakan pilih mode</p>
          @block([
            'class' => 'form-group row'
          ])
            @block([
              'class' => 'col-sm-2'
            ])
            @endblock
            @block([
              'class' => 'col-sm-10',
              'style' => 'text-align: left'
            ])
              @radio([
                'label' => 'Upload berkas dataset',
                'name' => 'mode_select',
                'id' => 'dataset_from_file',
                'value' => 'dataset_from_file',
                'custom' => true
              ])
              @endradio
              {{-- @radio([
                'label' => 'Generate random dataset',
                'name' => 'mode_select',
                'id' => 'random_dataset',
                'value' => 'random_dataset',
                'custom' => true
              ])
              @endradio --}}
            @endblock
          @endblock
          <form id="kmeans_form" action="{{route('kmeans.store')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="mode_selected" id="mode_selected" value="">
            {{-- <div id="dataset_random_value" style="display:none;">
              @text([
                'name' => 'num_of_gen_data',
                'value' => '',
                'label' => 'Jumlah data yang akan digenerate',
                'placeholder' => 'Masukkan jumlah data yang akan digenerate',
                'help_text' => 'Range : 1 - 999',
                'required' => true,
                'material' => true
              ])
              @endtext
            </div> --}}
            <div id="dataset_file" style="display: none;">
              <p>Upload berkas dataset yang akan diproses</p>
              @file([
                'name' => 'kmeans_source',
                'accept' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel',
                'help_text' => 'Maksimal 1MB',
                'required' => true
              ])
              @endfile
            </div>
            @text([
              'name' => 'num_of_cluster',
              'value' => '',
              'label' => 'Jumlah cluster',
              'placeholder' => 'Masukkan jumlah cluster',
              'help_text' => 'Range : 2 - 9',
              'required' => true,
              'material' => true
            ])
            @endtext
            <button type="submit" class="btn btn-primary">Proses !</button>
          </form>
        </div>
        <div class="copyrights text-center">
          <p>Design by <a href="https://bootstrapious.com/p/bootstrap-4-dashboard" class="external" target="_blank">Bootstrapious</a>, customized by Mainworks</p>
          <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
        </div>
      </div>
    </div>
  </div>
@endsection