@extends('layouts.app')

@section('title','Result Page')

@section('main')
  <div class="page container-fluid">
    <div class="container">
      <div class="content-header"></div>
      <div class="card">
        <div class="card-header">
          <div class="logo text-uppercase text-center"><a href="{{route('kmeans.index')}}"><span>Kmeans</span><strong class="text-primary">Clustering</strong></a></div>
        </div>
        <div class="card-body">
          @if (session()->get('message'))
            @alert([
              'type' => session()->get('type'),
              'message' => session()->get('message')
            ])
            @endalert
          @endif
          <div class="container-fluid row">
            <div class="col-sm-6">
              <div class="table-responsive">
                <div class="text-center">Dataset</div>
                <table class="table table-sm table-striped table-hover table-datatable">
                  <thead>
                    @columns
                      @column
                        #
                      @endcolumn
                      @column
                        X
                      @endcolumn
                      @column
                        Y
                      @endcolumn
                    @endcolumns
                  </thead>
                  <tbody>
                    @foreach ($datasets as $dataset)
                      @records
                        @record
                          {{$loop->iteration}}
                        @endrecord
                        @record
                          {{$dataset->x_value}}
                        @endrecord
                        @record
                          {{$dataset->y_value}}
                        @endrecord
                      @endrecords
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="table-responsive">
                <div class="text-center">Centroid position</div>
                <table class="table table-sm table-striped table-hover table-datatable">
                  <thead>
                    @columns
                      @column
                        #
                      @endcolumn
                      @column
                        X
                      @endcolumn
                      @column
                        Y
                      @endcolumn
                      @column
                        Items in cluster
                      @endcolumn
                    @endcolumns
                  </thead>
                  <tbody>
                    @if ($num_of_cluster > 1)
                      @foreach ($clusters as $i => $cluster)
                        @records
                          @record
                            {{$loop->iteration}}
                          @endrecord
                          @record
                            {{$cluster[0]}}
                          @endrecord
                          @record
                            {{$cluster[1]}}
                          @endrecord
                          @record
                            {{count($cluster)}}
                          @endrecord
                        @endrecords
                      @endforeach
                    @endif
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="line-break"></div>
          {{-- <div class="container-fluid row">
            <canvas id="kmeans_result"></canvas>
          </div> --}}
        </div>
      </div>
      <div class="copyrights text-center">
        <p>Design by <a href="https://bootstrapious.com/p/bootstrap-4-dashboard" class="external" target="_blank">Bootstrapious</a>, customized by Mainworks</p>
        <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
      </div>
    </div>
  </div>
@endsection
@push('js')
  {{-- <script>
    $(document).ready(function(){
      showChart({{$batch_number}},{{$num_of_cluster}});
    });
  </script> --}}
@endpush