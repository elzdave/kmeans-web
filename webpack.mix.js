const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');

/**
 * Vendor, template, and user specific CSS and JS mix
 */

mix.babel('resources/js/vendor/*.js','public/js/vendor.js')
   .babel('resources/js/theme/*.js','public/js/theme.js')
   .babel('resources/js/custom/*.js','public/js/custom.js');

mix.styles('resources/css/vendor/*.css','public/css/vendor.css')
   .styles('resources/css/theme/*.css','public/css/theme.css');

if (mix.inProduction()) {
   mix.version();
}